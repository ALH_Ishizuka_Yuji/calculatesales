package jp.alhinc.ishizuka_yuji.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String,InputData> branchList = new HashMap<String,InputData>();
		Map<String,InputData> commodityList = new HashMap<String, InputData>();
		List<String> fileList = null;
		String branchFileName = "branch.lst";
		String commodityFileName = "commodity.lst";
		String line;
		String directoryPath;
		String errorStr = null;
		File directory = null;
		LineNumberReader lr = null;

		try {
			//コマンドライン引数が1つ以外の場合の処理
			if (args.length != 1) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
			directory = new File(args[0]);
			directoryPath = args[0];
			fileList = Arrays.asList(directory.list());

			//1-1.支店定義ファイルの読み込み
			errorStr = readInputData(directoryPath, branchFileName, fileList, branchList, 0);
			if (errorStr != null) {
				System.out.println(errorStr);
				return;
			}

			//1-2.商品定義ファイルの読み込み
			errorStr = readInputData(directoryPath, commodityFileName, fileList, commodityList, 1);
			if (errorStr != null) {
				System.out.println(errorStr);
				return;
			}
		} catch (IOException readError) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		try {
			//2.集計処理
			//2-1.rcdファイルの抽出
			ArrayList<String> rcdList = new ArrayList<String>();
			for(String name : fileList) {
				if(name.matches("^\\d{8}\\.rcd$")) {
					rcdList.add(name);
				}
			}
			Collections.sort(rcdList);

			//2-2.rcdファイルの連番確認
			String[] check;
			int currentNumber;
			for(int i=0; i<rcdList.size(); i++) {
				check = rcdList.get(i).split("\\.");
				currentNumber = Integer.parseInt(check[0]);
				if(currentNumber != (i+1)) {
					//rcdファイルが連番ではない場合の処理
					System.out.println("売り上げファイルが連番になっていません");
					return;
				}
			}

			//2-3.rcdファイル読込、集計
			for(String rcd : rcdList) {
				File rcdData = new File(directoryPath,rcd);
				if (!rcdData.isFile()) {
					//.rcdがファイルではない場合の処理
					System.out.println("売り上げファイルが連番になっていません");
					return;
				}
				String branchCode = "", sales = "";
				int lineNumber;
				List<String> content = Files.readAllLines(
						FileSystems.getDefault().getPath(directoryPath, rcd));
				if (content.size() < 3) {
					//売上ファイルが3桁以下の場合の処理
					System.out.println(rcd+"のフォーマットが不正です");
					return;
				}
				lr = new LineNumberReader(new FileReader(rcdData));
				while((line = lr.readLine()) != null) {
					lineNumber = lr.getLineNumber();
					if(lineNumber == 1) {
						if(branchList.containsKey(line)) {
							branchCode = line;
						} else {
							//支店コードに該当がない場合の処理
							System.out.println(rcd+"の支店コードが不正です");
							return;
						}
					} else if(lineNumber == 2) {
						if (commodityList.containsKey(line)) {
							//3行目読み込み
							sales = lr.readLine();
							if(!sales.matches("^\\d+$")) {
								//売上金額が数字以外の場合の処理
								System.out.println("予期せぬエラーが発生しました");
								return;
							}
							branchList.get(branchCode).addSalesSum(Long.parseLong(sales));
							commodityList.get(line).addSalesSum(Long.parseLong(sales));
							int branchDigits = String.valueOf(branchList.get(branchCode).getSalesSum()).length();
							int commodityDigits = String.valueOf(commodityList.get(line).getSalesSum()).length();
							if (branchDigits > 10 || commodityDigits > 10) {
								//支店別集計合計金額が10桁超えた場合の処理
								System.out.println("売り上げ金額が10桁を超えました");
								return;
							}
						} else {
							//商品コードに該当がない例外処理
							System.out.println(rcd+"の商品コードが不正です");
							return;
						}
					} else {
						//売上ファイルが4桁以上の例外処理
						System.out.println(rcd+"のフォーマットが不正です");
						return;
					}
				}
			}
		} catch (IOException processError) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			try {
				if(lr != null) {
					lr.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}

		try {
			//3.集計結果出力
			//3-1.branchListを金額順にソート
			List<Map.Entry<String, InputData>> sortedBranchList =
					new ArrayList<Map.Entry<String, InputData>>(branchList.entrySet());
			Collections.sort(sortedBranchList, new Comparator<Map.Entry<String, InputData>>() {
				public int compare(Entry<String, InputData> entry1, Entry<String, InputData> entry2) {
					return ((Long)entry2.getValue().getSalesSum()).compareTo((Long)entry1.getValue().getSalesSum());
				}
			});

			//3-2.commodityListを金額順位にソート
			List<Map.Entry<String, InputData>> sortedCommodityList =
					new ArrayList<Map.Entry<String, InputData>>(commodityList.entrySet());
			Collections.sort(sortedCommodityList, new Comparator<Map.Entry<String, InputData>>() {
				public int compare(Entry<String, InputData> entry1, Entry<String, InputData> entry2) {
					return ((Long)entry2.getValue().getSalesSum()).compareTo((Long)entry1.getValue().getSalesSum());
				}
			});

			//3-3.店舗別集計ファイル書き込み
			File outputBranchData = new File(directoryPath,"branch.out");
			writeOutputData(outputBranchData, sortedBranchList);

			//3-4.商品別集計ファイル書き込み
			File outputCommodityData = new File(directoryPath,"commodity.out");
			writeOutputData(outputCommodityData, sortedCommodityList);

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}

	//読込共通メソッド
	private static String readInputData(String directoryPath, String fileName, List<String> directorylist,
			Map<String,InputData> dataList, int processId) throws IOException {
		File fileData = new File(directoryPath,fileName);
		 BufferedReader br = null;
		InputData data = new InputData();
		String[] wordList;
		String line, pattern, statement;

		if(directorylist.indexOf(fileName) == -1 ) {
			if (processId == 0) {
				//ディレクトリ内にbranch.lstがない場合の処理
				return "支店定義ファイルが存在しません";
			} else {
				//ディレクトリ内にcommodity.lstがない場合の処理
				return "商品定義ファイルが存在しません";
			}
		}
		if (processId == 0 ) {
			//支店定義ファイル読込
			statement = "支店定義ファイルのフォーマットが不正です";
			pattern = "^[0-9]{3}$";
		} else {
			//商品定義ファイル読込
			statement = "商品定義ファイルのフォーマットが不正です";
			pattern = "^[0-9a-zA-Z]{8}$";
		}
		//ファイル読込
		br = new BufferedReader(new FileReader(fileData));
		while((line = br.readLine()) != null) {
			wordList = line.split(",");
			if (wordList.length != 2) {
				//各定義ファイル1行の要素が2つでない場合の処理
				br.close();
				return statement;
			}
			if(wordList[0].matches(pattern)) {
				data.setCode(wordList[0]);
			} else {
				//各コードが仕様に沿わない場合の処理
				br.close();
				return statement;
			}
			if(!wordList[1].contains(",") && wordList[1].length() != 0) {
				data.setName(wordList[1]);
			} else {
				//各名前がない場合の処理
				br.close();
				return statement;
			}
			dataList.put(data.getCode(),data);
			data = new InputData();
		}
		if (br != null) {
			br.close();
		}
		return null;
	}

	//書込み共通メソッド
	private static void writeOutputData (File outputData, List<Entry<String, InputData>> dataList) throws IOException{
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputData));
		for(Entry<String, InputData> e : dataList) {
			bw.write(e.getValue().printInfo());
			bw.newLine();
		}
		bw.close();
	}

}
