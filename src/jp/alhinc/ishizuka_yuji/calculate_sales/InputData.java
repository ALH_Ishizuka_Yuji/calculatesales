package jp.alhinc.ishizuka_yuji.calculate_sales;

public class InputData {
	private String code;
	private String name;
	private long salesSum;

	public InputData() {
		code = "";
		name = "";
		salesSum = 0;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String ｎame) {
		this.name = ｎame;
	}
	public long getSalesSum() {
		return salesSum;
	}
	public void addSalesSum(long salesSum) {
		this.salesSum += salesSum;
	}
	public String printInfo() {
		String info = (code+","+name+","+salesSum);
		return info;
	}
}
